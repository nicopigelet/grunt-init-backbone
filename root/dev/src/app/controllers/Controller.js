
{%= namespace %}.Controller = function() {

	var
		eventHandlers = [],

		currentView = null,

		loadingView = null,
		homeView = null,
		
		/*
		 * init
		 * @private
		 */
		init = function() {

			_initEventHandlers();
			_initNav();
		},

		/*
		 * init event handler
		 * @private
		 */
		_initEventHandlers = function() {

			_.each( {%= namespace %}.Events, function(customEvent) {
				eventHandlers[customEvent] = _show;
			});
			
			{%= namespace %}.EventManager.bind(eventHandlers);
		},
		
		/*
		 * init navigation links
		 * @private
		 */
		_initNav = function() {

			$("body").delegate('a[rel=nav], nav a:not(.external)', "click", function(e){
				e.preventDefault();
				{%= namespace %}.AppRouter.navigate($(this).attr("href"), true);
			});
		},
		
		/*
		 * display Page
		 * @private
		 */
		displayPage = function ( callbackEvent, slug, hideFirst ) {

			if ( currentView && hideFirst ) {
				
				currentView.hide( function() {
					displayPage(callbackEvent, slug, false);
				});

			} else {

				{%= namespace %}.EventManager.trigger( {%= namespace %}.Events.APP_LOADING );
				{%= namespace %}.DataManager.check( callbackEvent, slug );
			}
		},

		/*
		 * show the page
		 * @private
		 */
		_show = function ( e /*, slug*/ ) {

			var view;
			
			switch ( e.type ) {
				
				case {%= namespace %}.Events.APP_LOADING :
					if ( !loadingView ) loadingView = new {%= namespace %}.View.Loading();
					view = loadingView;
				break;
				
				case {%= namespace %}.Events.SHOW_HOME :
					if ( !homeView ) {
						homeView = new {%= namespace %}.View.Home({
							items : {%= namespace %}.Data.Item
						});
					}
					view = homeView;
				break;

			}
			
			view.render();
			currentView = view;

		};

	init();
	return {
		displayPage : displayPage
	};
};
