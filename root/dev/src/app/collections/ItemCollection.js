
{%= namespace %}.Collection.ItemCollection = Backbone.Collection.extend({
	
	model : {%= namespace %}.Model.Item,
	url : "/data/items.json",
	
	initialize : function() {
		
	},

	parse : function(data){
		return data.items;
	}
	
});