
// Create Namespace
var {%= namespace %} = window.{%= namespace %} || {};

/* EVENT MANAGER */
{%= namespace %}.EventManager = {%= namespace %}.EventManager || $({});

/* COLLECTIONS */
{%= namespace %}.Collection = {%= namespace %}.Collection || {};

/* MODELS */
{%= namespace %}.Model = {%= namespace %}.Model || {};

/* VIEWS */
{%= namespace %}.View = {%= namespace %}.View || {};

/* DATA */
{%= namespace %}.Data = {%= namespace %}.Data || {};

/* LOCATIONS */
{%= namespace %}.Locations = {%= namespace %}.Locations || {};
{%= namespace %}.Locations.Templates = '/templates/';
{%= namespace %}.Locations.JSON = '/data/';

/*
 * EVENTS
 */
{%= namespace %}.Events = {
	APP_LOADING : "APP_LOADING",
    SHOW_HOME : "SHOW_HOME"
};

$(window).ready(function(){
	
	{%= namespace %}.AppRouter = new {%= namespace %}.Router();
	Backbone.history.start({ pushState : true });

});